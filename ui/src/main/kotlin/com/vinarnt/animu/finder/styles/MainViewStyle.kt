package com.vinarnt.animu.finder.styles

import tornadofx.*


class MainViewStyle : Stylesheet() {

    companion object {

        val mainLayout by cssid()
    }

    init {

        mainLayout {

            backgroundColor += c("#404040")
        }
    }
}