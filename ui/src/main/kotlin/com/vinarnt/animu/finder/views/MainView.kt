package com.vinarnt.animu.finder.views

import javafx.scene.control.Button
import javafx.scene.layout.BorderPane
import org.controlsfx.control.textfield.CustomTextField
import tornadofx.*


class MainView : View("Animu Finder") {

    override val root: BorderPane by fxml()

    private val searchButton: Button by fxid()

    private val settingsButton: Button by fxid()

    private val searchBar: CustomTextField by fxid()

    private val popularView: PopularGridView by inject()

    init {

        root.center = popularView.root
    }


    fun onSearchAction() {

        val searchQuery = searchBar.text
        println("Search for: $searchQuery")
    }

    fun onSettingsAction() {

        println("Settings")
    }
}