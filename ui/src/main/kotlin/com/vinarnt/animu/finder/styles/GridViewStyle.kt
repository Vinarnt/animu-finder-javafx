package com.vinarnt.animu.finder.styles

import javafx.geometry.Pos
import tornadofx.*


class GridViewStyle : Stylesheet() {

    companion object {

    }

    init {

        datagridCell  {

            backgroundColor += c("transparent")

            and(hover) {

                backgroundColor += c("#000000")
                translateY = 5.px
            }

            title {

                backgroundColor += c("transparent")
                alignment = Pos.CENTER
                textFill = c("#FFFFFF")
            }
        }
    }
}