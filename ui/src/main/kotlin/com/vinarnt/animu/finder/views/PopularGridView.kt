package com.vinarnt.animu.finder.views

import com.sun.javafx.scene.control.skin.VirtualScrollBar
import com.vinarnt.animu.finder.service.MALService
import javafx.geometry.Pos
import javafx.scene.input.MouseEvent
import tornadofx.*


class PopularGridView : View("Popular datagrid") {

    private val dataService: MALService by di()
    private var loadingNextData: Boolean = false
    private var loadedToPage = 1

    private val cellWidth = 170.0
    private val cellHeight = 250.0

    override val root = datagrid(dataService.getPopularAnimes(loadedToPage)) {

        cellWidth = this@PopularGridView.cellWidth
        cellHeight = this@PopularGridView.cellHeight

        cellCache {

            val title = it.title

            vbox(null, Pos.CENTER) {

                imageview(it.imageUrl) {

                    fitWidthProperty().bind(cellWidthProperty)
                    fitHeight = cellHeight - 25
                }
                label(it.title) {

                    styleClass += "title"
                    maxHeight = 25.0
                    maxWidth = cellWidth
                }

                addEventHandler(MouseEvent.MOUSE_CLICKED) {

                    println("Show anime $title")
                }
            }
        }

        runLater {

            val vScrollBar = this@datagrid.lookup(".scroll-bar") as VirtualScrollBar
            vScrollBar.valueProperty().onChange {

                if (it == 1.0 && !loadingNextData) {

                    loadingNextData = true

                    runAsync {

                        dataService.getPopularAnimes(++loadedToPage)

                    } ui {

                        if (it != null)
                            this@datagrid.items.addAll(it)

                        loadingNextData = false
                    }
                }
            }
        }
    }
}
