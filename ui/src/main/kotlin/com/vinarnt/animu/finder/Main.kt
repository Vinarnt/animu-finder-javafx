package com.vinarnt.animu.finder

import com.vinarnt.animu.finder.service.MALService
import com.vinarnt.animu.finder.styles.GridViewStyle
import com.vinarnt.animu.finder.styles.MainViewStyle
import com.vinarnt.animu.finder.views.MainView
import tornadofx.*
import kotlin.reflect.KClass

class Main : App(MainView::class, MainViewStyle::class, GridViewStyle::class) {

    init {

        FX.dicontainer = object : DIContainer {

            override fun <T : Any> getInstance(type: KClass<T>): T =

                    when (type) {

                        MALService::class -> MALService() as T
                        else -> throw ClassNotFoundException("Class of type ${type.qualifiedName} not found")
                    }
        }
    }
}