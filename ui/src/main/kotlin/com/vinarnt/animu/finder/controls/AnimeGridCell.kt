package com.vinarnt.animu.finder.controls

import com.vinarnt.scraper.model.anime.AnimeTopEntry
import org.controlsfx.control.GridCell


class AnimeGridCell : GridCell<AnimeTopEntry>() {

    override fun updateItem(item: AnimeTopEntry?, empty: Boolean) {

        super.updateItem(item, empty)

        text = item?.title
    }
}