package com.vinarnt.animu.finder.service

import com.vinarnt.scraper.MALScraper


class MALService {

    private val scraper = MALScraper()

    fun getPopularAnimes(page: Int) = scraper.getPopularAnimes(page)
}